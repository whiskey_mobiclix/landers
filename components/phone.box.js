function sendPin({requestId, phone}){
	const data = {
		lang: LANDER.lang || "en",
		msisdn: handlePhoneNumberWithPrefix({
			phone,
			prefix: "47",
		}),
		request_id: requestId,
	};

	tracking({
		service: API_URIS.sendPin,
		status: "requesting",
		request: data,
	})

	return new Promise(function(resolve, reject){
		axios.post(API_URIS.sendPin, data).then(res => {
			tracking({
				service: API_URIS.sendPin,
				status: "success",
				reponse: res.data,
			});
			resolve(res);
		}).catch(e => {
			tracking({
				service: API_URIS.sendPin,
				status: "failed",
				reponse: e.response || e,
			});
			reject(e);
		})
	});
}

const PhoneBox = ({requestId, onSuccess}) => {
	var container = document.createElement("div"),
		wrapper = document.createElement("div"),
		label = document.createElement("div"),
		prefix = document.createElement("span"),
		input = document.createElement("input"),
		button = document.createElement("button"),
		error = document.createElement("div"),
		step = document.createElement("div"),
		loading = false;

	wrapper.classList.add("wrapper");

	label.classList.add("label");
	label.innerHTML = "Telefonnummeret mitt er";

	input.type = "tel";
	input.placeholder = "Hva er telefonnummeret ditt";
		
	prefix.innerHTML = "+47";

	step.classList.add("step");
	step.innerHTML = "Steg 1/2" + "<br />(" + LANDER.price + " " + LANDER.currency + "/" + getPackageName() +"). Kundeservice (+47) 22120331";

	error.classList.add("phone-error");
	error.classList.add("error");
	error.style.display = "none";

	button.classList.add("cta");
	button.innerHTML = "Neste steg";
	button.onclick = function(){
		handleSendPin();
	}

	input.onkeyup = function(e){
		if(e.which == 13){
			handleSendPin();
		}
	}

	const handleSendPin = async function(){
		if(loading) return;

		if(!input.value) {
			wrapper.classList.add("has-error");
			return;
		}

		loading = true;
		input.readOnly = true;
		error.style.display = "none";
		button.innerHTML = "Sender...";
		wrapper.classList.remove("has-error");

		try{
			const response = await sendPin({
				requestId,
				phone: input.value,
			});

			if(response.data.code < 0){
				handleError(response.data);
			} else if(onSuccess) {
				onSuccess(response.data.data);
			}
		} catch(e){
			console.log(e);
			handleError();
		}
	}

	const handleError = function(data){
		loading = false;
		input.readOnly = false;
		var errorMessage = "Noe gikk galt når du sendte OTP til telefonnummeret ditt. Kan du prøve igjen?";
		try{
			errorMessage += "<br />" + "<span>Code: " + data.code + " - Message: " + data.error.message + "</span>"; 
		} catch(e){
			console.log(e);
		}
		error.innerHTML = errorMessage;
		error.style.display = "block";
		button.innerHTML = "Neste steg";
	}

	wrapper.appendChild(prefix);
	wrapper.appendChild(input);
	container.appendChild(label);
	container.appendChild(wrapper);
	container.appendChild(button);
	container.appendChild(step);
	container.appendChild(error);

	return container;
}
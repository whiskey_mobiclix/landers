function verifyPin({requestId, transId, pin, phone}){
	const data = {
		pin,
		request_id: requestId,
		trans_id: transId,
		msisdn: phone,
	};

	tracking({
		service: API_URIS.verifyPin,
		status: "requesting",
		request: data,
	})

	return new Promise(function(resolve, reject){
		axios.post(API_URIS.verifyPin, data).then(res => {
			tracking({
				service: API_URIS.verifyPin,
				status: "success",
				response: res.data,
			});
			resolve(res);
		}).catch(e => {
			tracking({
				service: API_URIS.verifyPin,
				status: "failed",
				response: e.response || e,
			});
			reject(e);
		});
	});
}

const PinBox = ({requestId, transId, phone, onSuccess}) => {
	var container = document.createElement("div"),
		label = document.createElement("div"),
		wrapper = document.createElement("div"),
		input = document.createElement("input"),
		button = document.createElement("button"),
		error = document.createElement("div"),
		step = document.createElement("div"),
		loading = false;

	label.classList.add("label");
	label.innerHTML = "Jeg mottok denne koden"

	wrapper.classList.add("wrapper");

	input.type = "tel";
	input.placeholder = " ____";
	
	error.classList.add("phone-error");
	error.classList.add("error");
	error.style.display = "none";

	step.classList.add("step");
	step.innerHTML = "Steg 2/2" + "<br />(" + LANDER.price + " " + LANDER.currency + "/" + getPackageName() +"). Kundeservice (+47) 22120331";

	button.classList.add("cta");
	button.innerHTML = "Bekrefte!";
	button.onclick = function(){
		handleVerifyPin();
	}

	input.onkeyup = function(e){
		if(e.which == 13){
			handleVerifyPin();
		}
	}

	const handleVerifyPin = async function(){
		if(loading) return;
			
		if(!input.value){
			input.classList.add("has-error");
			return;
		}

		loading = true;
		input.readOnly = true;
		input.classList.remove("has-error");
		button.innerHTML = "Sender...";
		error.style.display = "none";

		try{
			const reponse = await verifyPin({
				pin: input.value,
				requestId,
				transId,
				phone,
			});

			if(reponse.data.code < 0){
				handleError(reponse.data);
			} else {
				onSuccess && onSuccess(reponse.data.data);
			}
		} catch(e){
			console.log(e);
			handleError();
		}

	}

	const handleError = function(data){
		loading = false;
		input.readOnly = false;
		var errorMessage = "Noe gikk galt da du bekreftet PIN-koden din. Kan du prøve igjen?";
		try{
			errorMessage += "<br />" + "<span>Code: " + data.code + " - Message: " + data.error.message + "</span>"; 
		} catch(e){
			console.log(e);
		}
		error.innerHTML = errorMessage;
		error.style.display = "block";
		button.innerHTML = "Bekrefte!";
	}

	container.appendChild(label);
	wrapper.appendChild(input);
	container.appendChild(wrapper);
	container.appendChild(button);
	container.appendChild(step);
	container.appendChild(error);

	return container;
}
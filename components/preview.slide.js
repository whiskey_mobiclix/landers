const PreviewSlide = data => {
	var container = document.createElement("div"),
		wrapper = document.createElement("div"),
		images = document.createElement("div"),
		nav = document.createElement("div"),
		active = 0;

	wrapper.classList.add("wrapper");
	nav.classList.add("nav");
	images.classList.add("images");

	wrapper.style.width = (data.length + 1) * 100 + "%";

	var firstItem = document.createElement("div");
	firstItem.classList.add("slide-item");

	for(var i=0; i<data.length;i++){
		const button = document.createElement("button");
		button.classList.add("nav-button");
		button.setAttribute("ix", i);
		// button.onclick = function(){
		// }

		if(i==active) button.classList.add("active");

		const item = document.createElement("div"),
			img = document.createElement("img"),
			text = document.createElement("div");

		item.classList.add("slide-item");
		item.style.width = 100 / (data.length + 1) + "%";

		img.src = data[i].image;

		text.classList.add("slide-text");
		text.innerHTML = data[i].text;

		item.appendChild(img);
		item.appendChild(text);


		images.appendChild(item);
		nav.appendChild(button);

		if(i==0){
			firstItem = item.cloneNode(true);
		}
	}

	images.appendChild(firstItem);
	wrapper.appendChild(images);
	container.classList.add("preview-slide");
	container.appendChild(wrapper);
	container.appendChild(nav);

	var intervalHandler = window.setInterval(() => {
		const slide = document.getElementById("slide");
		console.log("Slide change");
		
		if(!slide){
			window.clearInterval(intervalHandler);
		}

		active += 1;
		const navButtons = document.getElementsByClassName("nav-button");
		
		for(var k=0; k<navButtons.length; k++){
			const ix = parseInt(navButtons[k].getAttribute("ix"));
			if(ix == active){
				navButtons[k].classList.add("active");
			} else {
				navButtons[k].classList.remove("active");
			}
		}

		if(active == data.length){
			wrapper.style.marginLeft = -1 * data.length * 100 + "%";
			active = 0;
			for(var k=0; k<navButtons.length; k++){
				const ix = parseInt(navButtons[k].getAttribute("ix"));
				if(ix == active){
					navButtons[k].classList.add("active");
				} else {
					navButtons[k].classList.remove("active");
				}
			}

			setTimeout(() => {
				wrapper.classList.add("no-transition");
				wrapper.style.marginLeft = "0%";

				setTimeout(() => {
					wrapper.classList.remove("no-transition");
				}, 200);
			}, 300);
		} else {
			wrapper.style.marginLeft = -1 * active * 100 + "%";
		}
	}, 2500);

	return container;
}
function init(){
	var data = {
		aggregator: LANDER.aggregator,
		callback: LANDER.callback,
		lang: LANDER.lang || "en",
		market: LANDER.market,
		operator: LANDER.operator || "",
		origin_url: window.location.href,
		package: LANDER.package,
		product: LANDER.product,
		request_from: "lp",
		...parseQuery(),
	};

	tracking({
		service: API_URIS.init,
		status: "requesting",
		request: data,
	});

	return new Promise(function(resolve, reject){
		axios.post(API_URIS.init, {
			...data,
		}).then(res => {
			tracking({
				service: API_URIS.init,
				status: "success",
				reponse: res.data,
			});
			resolve(res);
		}).catch(e => {
			tracking({
				service: API_URIS.init,
				status: "failed",
				reponse: e.reponse || e,
			});
			reject(e);
		})
	});
}

function subscribe(data){
	tracking({
		service: API_URIS.subscribe,
		status: "requesting",
		request: data,
	});

	return new Promise(function(resolve, reject){
		axios.post(API_URIS.subscribe, data).then(res => {
			tracking({
				service: API_URIS.subscribe,
				status: "success",
				response: res.data,
			});
			resolve(res);
		}).catch(e => {
			tracking({
				service: API_URIS.subscribe,
				status: "failed",
				response: e.response || e,
			});
			reject(e);
		});
	});
}

const SubscribeButton = ({text, onSuccess, onError}) => {
	const loadingText = LANDER.lang == "cs" ? "Načítání..." : "Laster inn...";

	var div = document.createElement("div"),
		button = document.createElement("button"),
		error = document.createElement("div"),
		price = document.createElement("div"),
		loading = false;

	error.classList.add("subscribe-error");
	error.classList.add("error");
	error.style.display = "none";
	

	var priceText = LANDER.price + " " + LANDER.currency + "/" + getPackageName();
	if(LANDER.aggregator == "strex"){
		priceText += "<br />Kundeservice (+47) 22120331";
	}

	price.classList.add("price");
	price.innerHTML = priceText;

	button.classList.add("cta");
	button.innerHTML = text;
	button.onclick = function(){
		if(loading) return;

		loading = true;
		error.style.display = "none";
		button.innerHTML = loadingText;

		handleInit({
			onSuccess: res => {
				if(LANDER.aggregator == "strex"){
					handleSubscribe({responseDataFromInitStep: res});
				} else {
					handleRedirect({responseDataFromInitStep: res});
				}
			}
		})
	}

	const handleInit = async function({onSuccess}) {
		try{
			const response = await init();

			if(response.data.code < 0){
				handleError();
			} else {
				onSuccess(response.data);
			}
		} catch (e){
			console.log(e);
			handleError();
		}
	}

	const handleSubscribe  = async function({responseDataFromInitStep}){
		try{
			if(responseDataFromInitStep.data.aggregator == "strex"){
				const url = responseDataFromInitStep.data.url, search = url.substring(url.indexOf("?") + 1);
				const params = getParamsFromSearchQuery(search);
				var {stage, ...data} = params;

				const response = await subscribe(data);
				if(response.data.code < 0){
					handleError();
				} else if(onSuccess) {
					onSuccess(response.data.data);
				}
			}
		} catch(e){
			console.log(e);
			handleError();
		}
	}

	const handleRedirect = function({responseDataFromInitStep}){
		if(LANDER.aggregator == "centili" && LANDER.market == "cz" && LANDER.operator == "o2"){
			const iframe = document.createElement("iframe");
			iframe.url = responseDataFromInitStep.data.url;
			iframe.id = "iframe";
			iframe.style.display = "none";
			iframe.onload = function(){
				console.log("Redirect success!");
				setTimeout(async function(){
					try{
						const response = await subscribe({
							...parseQuery(),
							request_id: responseDataFromInitStep.data.request_id,
						});

						if(response.data.code < 0){
							handleError(response.data);
						} else if(onSuccess) {
							onSuccess(response.data.data);
						}
					} catch(e){
						handleError();
					}
				}, 500);
			}

			document.getElementsByTagName("body")[0].appendChild(iframe);
		}
	}

	const handleError = function(data){
		loading = false;
		button.innerHTML = text;
		var message = LANDER.lang == "cs" ? "Při přihlášení k odběru se něco pokazilo. Můžete to zkusit znovu, prosím?" : "Noe gikk galt når du abonnerer. Kan du prøve igjen, vær så snill?";
		if(data){
			error.classList.add("has-data");
			message += "<span>Code: " + data.code + " - Message: " + data.error.message + "</span>"
		} else {
			error.classList.remove("has-data");
		}
		error.innerHTML = message;
		error.style.display = "block";
		onError && onError();
	}

	div.appendChild(button);
	div.appendChild(price);
	div.appendChild(error);

	return div;
}
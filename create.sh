RED='\033[0;31m'
YELLOW='\033[1;33m'
NC='\033[0m'

if [[ $1 = -help ]]; then
	echo "${YELLOW}./create.sh [product] [lander_id]${NC}"
else
	if [ -z $1 ]; then
		echo "${RED}Missing product name${NC}\n${YELLOW}./create.sh [product] [lander_id]${NC}"
	else
		if [ -z $2 ]; then
			echo "${RED}Missing lander id${NC}\n${YELLOW}./create.sh [product] [lander_id]${NC}"
		else
			mkdir -p ./pages/$1
			mkdir ./pages/$1/$2
			cp -rf ./build/* ./pages/$1/$2
		fi
	fi
fi
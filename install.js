function readTextFile(path, onSuccess){
    var rawFile = new XMLHttpRequest();
    rawFile.open("GET", path, false);
    rawFile.onreadystatechange = function ()
    {
        if(rawFile.readyState === 4)
        {
            if(rawFile.status === 200 || rawFile.status == 0)
            {
                var text = rawFile.responseText;
                onSuccess && onSuccess(text);
            }
        }
    }
    rawFile.send(null);
}

function addFaviconToHeadTag(product){
	const favicon = BASE64_FAVICONS[product];

	var link = document.createElement("link");
	link.rel = "icon";
	link.type = "image/png";
	link.href = favicon;
	document.getElementsByTagName('head')[0].appendChild(link);
}

function addAxiosScriptToHeadTag(){
	var script = document.createElement("script");
	script.type = "text/javascript";
	script.src = AXIOS_CDN;
	document.getElementsByTagName('head')[0].appendChild(script);
}

function addScriptsToBodyTagViaFilePath(path){
	readTextFile(path, text => {
		const script = document.createElement("script");
		script.type = "text/javascript";
		script.innerHTML = text;
		document.getElementsByTagName('body')[0].appendChild(script);
	});
}

function addStyleSheetsToHeadTag(path, id){
	readTextFile(path, text => {
		const css = document.createElement("style");
		css.type = "text/css";
		css.innerHTML = text;
		if(id) css.setAttribute("id", id);
		document.getElementsByTagName('head')[0].appendChild(css);
	});
}

function addScriptToBodyTag(){
	var script = document.createElement("script");
	script.type = "text/javascript";
	script.src = "./script.js";
	script.setAttribute("id", "app-script");
	document.getElementsByTagName('body')[0].appendChild(script);
}

function addAppContentToBodyTag(){
	readTextFile("./app.html", text => {
		const app = document.createElement("div");
		app.setAttribute("id", "app");
		app.innerHTML = text;
		document.getElementsByTagName('body')[0].prepend(app);
	});
}

function download(data, filename, type) {
    var file = new Blob([data], {type: type});
    if (window.navigator.msSaveOrOpenBlob) // IE10+
        window.navigator.msSaveOrOpenBlob(file, filename);
    else { // Others
        var a = document.createElement("a"),
                url = URL.createObjectURL(file);
        a.href = url;
        a.download = filename;
        document.body.appendChild(a);
        a.click();
        setTimeout(function() {
            document.body.removeChild(a);
            window.URL.revokeObjectURL(url);  
        }, 0); 
    }
}

function addExportButton(){
	const button = document.createElement("button");
	button.setAttribute("id", "export-button");
	button.innerHTML = "Export this lander";
	document.getElementsByTagName('body')[0].appendChild(button);

	button.onclick = function(){
		const app = document.getElementById("app");
		readTextFile("./app.html", text => {
			app.innerHTML = text;
		});
		removeDOMElementById("export-button");
		removeDOMElementById("app-stylesheet");

		const html = document.getElementById("html");
		download("<html>" + html.innerHTML + "</html>", LANDER.id + ".html", "text/html");
		window.location.reload(true);
	}
}

function generate(){
	addAppContentToBodyTag();
	addStyleSheetsToHeadTag("./style.css");
	addStyleSheetsToHeadTag("../../../style.css", "app-stylesheet");
	addScriptsToBodyTagViaFilePath("../../../landers.config.js");
	addFaviconToHeadTag(LANDER.product);
	addAxiosScriptToHeadTag();
	addScriptsToBodyTagViaFilePath("../../../utils.js");
	addScriptsToBodyTagViaFilePath("../../../components/subscribe.button.js");
	addScriptsToBodyTagViaFilePath("../../../components/phone.box.js");
	addScriptsToBodyTagViaFilePath("../../../components/pin.box.js");
	addScriptsToBodyTagViaFilePath("../../../components/preview.slide.js");
	addScriptsToBodyTagViaFilePath("../../../common.js");
	addScriptsToBodyTagViaFilePath("./script.js");
	removeDOMElementById("install-script-file");
	removeDOMElementById("install-script-run");
	addExportButton();
}